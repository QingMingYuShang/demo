package dao.impl;

import dao.BaseDAO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.jdbc.ReturningWork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.util.ObjectUtils;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

/**
 * @Author : lq
 * @Description :baseDao接口实现类
 * @Date :
 */
@Repository("BaseDao")
public class BaseDAOImpl<T,ID extends Serializable> extends HibernateDaoSupport implements BaseDAO<T,ID> {
    private static final Logger logger = LoggerFactory.getLogger(BaseDAOImpl.class);
    @Autowired
    private SessionFactory sessionFactory;

    /*
    *@Author : wuqingzhou
    *@Description : 获取session
    *@param : []
    *@return: org.hibernate.Session
    *@Date : 2017/3/25 12:33
   */
    @Override
    public Session getSession()
    {
        return sessionFactory.getCurrentSession();
    }
    /*
     *@Author : wuqingzhou
     *@Description : 保存单个实体
     *@param : [entity]  单个实体
     *@return: boolean  返回是否保存成功
     *@Date : 2017/3/25 12:34
     */
    @Override
    public boolean save(T entity) {
        this.getHibernateTemplate().save(entity);
        return true;
    }
    /*
     *@Author : wuqingzhou
     *@Description : 保存或者更新单个实体
     *@param : [o]  任意对象
     *@return: void  无返回值
     *@Date : 2017/3/25 12:39
     */
    public void saveOrUpdate(Object o) {
        this.getHibernateTemplate().saveOrUpdate(o);
    }
    /*
     *@Author : wuqingzhou
     *@Description : 修改单个实体
     *@param : [o]  任意单个实体
     *@return: void  无返回值
     *@Date : 2017/3/25 12:35
     */
    public void update(Object o) {
        this.getHibernateTemplate().update(o);
    }
    /*
     *@Author : wuqingzhou
     *@Description : 保存并更新实体
     *@param : [o]  任意对象
     *@return: void   无返回值
     *@Date : 2017/3/25 12:38
    */
    public void merge(Object o){
        this.getHibernateTemplate().merge(o);
    }
    /**
     * @Author : Lin Can
     * @Description : 更新并返回一个对象
     * @Date_Created : 2017/4/24 10:51
     * @Param o
     * @Return :
     */
    public Object mergeAndReturnObject(Object o){
        Object obj = this.getHibernateTemplate().merge(o);
        getHibernateTemplate().flush();
        getHibernateTemplate().clear();
        return obj;
    }
    public void UpdateBill(Object o){
        this.getHibernateTemplate().update(o);
        getHibernateTemplate().flush();
        getHibernateTemplate().clear();
        //return obj;
    }

    @Override
    public void updateSQL(String sql) {
        Session session =  this.getSession();
        Query query = session.createSQLQuery(sql);
        query.executeUpdate();
    }

    /*
         *@Author : wuqingzhou
         *@Description : 删除单个实体
         *@param : [o]任意对象
         *@return: void  无返回值
         *@Date : 2017/3/25 12:39
         */
    public void delete(Object o) {
        if (o == null) {
            return;
        }
        this.getHibernateTemplate().delete(o);
    }
    /**
     * @author : Lin Can
     * @description : 用于批量删除集合中的数据
     * @dateCreated : 2018/3/20 17:21
     * @Param o
     * @Return :
     */
    public void deleteAll(Collection<T> entities) {
        this.getHibernateTemplate().deleteAll(entities);
    }
    /*
     *@Author : wuqingzhou
     *@Description : 根据类的Class删除实体所有数据
     *@param : [tClass]
     *@return: void
     *@Date : 2017/3/25 12:42
     */
    public void deleteAll(Class<T> tClass){
        String hql="delete from "+tClass.getName()+" where 1=1";
        Session session=sessionFactory.getCurrentSession();
        session.createQuery(hql).executeUpdate();
    }
    /**
     * @author : Lin Can
     * @description : 根据id集合和id的名称删除数据
     * @dateCreated : 2018/3/21 9:00
     * @Param tClass 类名
    * @Param conditionName id名
    * @Param conditionValues  id集合
     * @Return :
     */
    public void deleteAllByIds(Class<T> tClass,String conditionName,Collection conditionValues){
        if (CollectionUtils.isEmpty(conditionValues)) {
            return;
        }
        if (StringUtils.isBlank(conditionName)) {
            conditionName = "id";
        }

        StringBuilder inIds = new StringBuilder();
        Iterator valIterator = conditionValues.iterator();
        while (valIterator.hasNext()) {
            inIds.append(",'");
            inIds.append(valIterator.next());
            inIds.append("'");
        }

        String sql="delete from "+tClass.getName() + " where " + conditionName + " in(" + inIds.toString().substring(1)+ ")";
        Session session=sessionFactory.getCurrentSession();
        session.createQuery(sql).executeUpdate();
    }
    /**
     * @author : Lin Can
     * @description : 根据条件删除数据
     * @dateCreated : 2018/2/5 9:19
     * @Param tClass 实体类名
    * @Param condition 匹配条件
     * @Return :
     */
    @Override
    public void deleteByCondition(Class<T> tClass,String conditionName,String conditionValue) {
        String hql="delete from "+tClass.getName() + " where " + conditionName + "='" + conditionValue + "'";
        Session session=sessionFactory.getCurrentSession();
        session.createQuery(hql).executeUpdate();
    }
    /*
       * @Author : panjiabao
       * @Description : 批量保存实体对象
       * @param : List  实体对象集合
       * @return :  void  无返回值
       * @Date : 2017/3/21 12:24
       * @modified by: Lin Can  方便和saveAll区分并且更加明确两个方法的不同，所以将此方法改名
       * merge在保存之前会先查询该对象在session中是否存在，不存在再向数据库发起查询
       * 而save直接保存
      */
   // public void insertAll(List<T> objectList) {
    public void mergeAll(List<T> objectList) {
        int i = 0;
        Session session = sessionFactory.getCurrentSession();
        for (T o : objectList) {
            session.merge(o);
            i++;
            if (i % 10000 == 0) {
                // 只是将Hibernate缓存中的数据提交到数据库，保持与数据库数据的同步
                session.flush();
                // 清除内部缓存的全部数据，及时释放出占用的内存
                session.clear();
            }

        }
        session.flush();
        session.clear();
    }


    /**
     * @author : Lin Can
     * @description : 批量保存对象到数据库
     * 和merge的区别是保存时即使对象包含id也不用先从数据库中查询，而是直接保存
     * 适用于保存跨境电商的表体数据
     * @dateCreated : 2018/3/19 11:08
     * @Param objectList
     * @Return :
     */
    public void saveAll(List<T> objectList) {
        int i = 0;
        Session session = sessionFactory.getCurrentSession();
        for (T o : objectList) {
            session.save(o);
            i++;
            if (i % 10000 == 0) {
                // 只是将Hibernate缓存中的数据提交到数据库，保持与数据库数据的同步
                session.flush();
                // 清除内部缓存的全部数据，及时释放出占用的内存
                session.clear();
            }

        }
        session.flush();
        session.clear();
    }
    /*
    /*
       * @Author : panjiabao
       * @Description : 批量更新实体对象
       * @param : list 实体对象集合
       * @return : void  无返回值
       * @Date : 2017/3/22 10:21
      */
    public void updateAll(List<T> objectList) {
        int i = 0;
        Session session = sessionFactory.getCurrentSession();
        for (T o : objectList) {
            session.update(o);
            i++;
            if (i % 50 == 0) {
                // 只是将Hibernate缓存中的数据提交到数据库，保持与数据库数据的同步
                session.flush();
                // 清除内部缓存的全部数据，及时释放出占用的内存
                session.clear();
            }
            session.flush();
            session.clear();
        }
    }
    /*
     *@Author : wuqingzhou
     *@Description : 根据类的Class  和 实体对象的ID 删除单个实体
     *@param : [tClass, id] 类的Class 实体对象的ID
     *@return: void 无返回值
     *@Date : 2017/3/25 13:04
     */
    public void removeByID(Class<T> tClass, ID id) {
        Session session = sessionFactory.getCurrentSession();
        Object persistentInstance = session.get(tClass, id);
        if (persistentInstance != null) {
            session.delete(persistentInstance);
            logger.info("deleteById entity by id successfully!");
        }else{
            logger.warn("Attempt to deleteById a not existed entity!");
        }
    }
    /*
   *@Author : panjiabao
   *@Description : HQL语句查询单个实体
   *@param : String hql hql语句
   *@return: T  单个实体
   *@Date : 2017/3/20 9:46
   */
    public T get(String hql) {
        List list = this.getHibernateTemplate().find(hql);
        if(list.size()==0){
            return null;
        }
        else{
            return (T)list.get(0);
        }

    }

    /*
     *@Author : wuqingzhou
     *@Description : 根据hql语句和对象的实例，获取单个对象
     *@param : [hql, obj]  hql语句  实体对象
     *@return: java.lang.Object  返回一个对象
     *@Date : 2017/3/25 13:10
     */
    public Object get(String hql, Object obj) {
        List list = this.getHibernateTemplate().find(hql,obj);
        if(list.size()==0){
            return null;
        }
        else{
            return list.get(0);
        }
    }
    /*
       *@Author : wuqingzhou
       *@Description : 根据hql语句查询一个对象的集合
       *@param : [hql] hql语句
       *@return: java.util.List  对象的集合
       *@Date : 2017/3/25 13:12
       */
    public List find(String hql) {
        System.out.println("hibernateTemplate is "+this.getHibernateTemplate());
        return this.getHibernateTemplate().find(hql);
    }

    /**
     * @author : Lin Can
     * @description : 更加实体的数据查询符合条件的集合
     * @dateCreated : 2018/5/22 17:20
     * @Param bean
     * @Return :
     */
    public List<T> find(T bean) {
        System.out.println("hibernateTemplate is "+this.getHibernateTemplate());
        return this.getHibernateTemplate().findByExample(bean);
    }
    @Override
    public List find (String hql,String paramName,Collection list) {
        Query query = getSession().createQuery(hql);
        query.setParameterList(paramName, list);
        return query.list();
    }

    /*
      *@Author : wuqingzhou
      *@Description : 根据hql语句和对象的实例 ，获取一个对象的集合
      *@param : [hql, obj] hql语句 任意对象
      *@return: java.util.List 返回一个对象的集合
      *@Date : 2017/3/25 13:13
      */
    public List find(String hql, Object obj) {
        return this.getHibernateTemplate().find(hql,obj);
    }
    /*
     *@Author : wuqingzhou
     *@Description : 根据类的Class  和  对象的ID查询单个对象
     *@param : [entity, id]类的Class 对象的ID
     *@return: java.lang.Object 返回单个对象
     *@Date : 2017/3/25 13:16
     */
    public Object findById(Class<?> entity, ID id){
        Object instance = getHibernateTemplate().get(entity, id);
        return instance;
    }
    /*
     *@Author : wuqingzhou
     *@Description : 根据类的Class 查询所有的实例
     *@param : [classEntity] 类的Class
     *@return: java.util.List<T> 返回所有的实例对象
     *@Date : 2017/3/25 13:19
     */
    public List<T> findAll(Class<?> classEntity){

        return find("From " + classEntity.getSimpleName());
    }
    /*
     *@Author : wuqingzhou
     *@Description : 根据sql语句查询任意对象
     *@param : [sql] sql语句
     *@return: java.util.List<T> 任意对象的list集合
     *@Date : 2017/3/25 13:23
     */
    @Override
    public List<?> findSQL(final String sql){
        Session session = this.getSession();
        Query query = session.createSQLQuery(sql);
        List<?> list=query.list();
        return list;
    }
    /*
     *@Author : wuqingzhou
     *@Description : 根据sql语句进行分页查询
     *@param : [sql, page, pageSize] sql语句 当前页数 每页显示最大数量
     *@return: java.util.List<T> 任意对象的集合
     *@Date : 2017/3/25 13:25
     */
    @Override
    public List<T> findSQLPage(final String sql, int page, int pageSize){
        Session session =  this.getSession();
        Query query = session.createSQLQuery(sql);
        int firstResult = (page - 1) * pageSize;
        query.setFirstResult(firstResult);
        query.setMaxResults(pageSize);
        List<T> list=query.list();
        return list;
    }
    /*
     *@Author : wuqingzhou
     *@Description : 根据hql语句统计数据的条数
     *@param : [hql] hql语句
     *@return: int 数量
     *@Date : 2017/3/25 13:29
     */
    @Override
    public int count(String hql) {
        if (StringUtils.isEmpty(hql)){
            throw new IllegalArgumentException("hql is null");
        }
        Object result = getSession().createQuery(hql).uniqueResult();
        if(result==null){
            return 0;
        }
        return ((Long)result).intValue();
    }
    /*
     *@Author : wuqingzhou
     *@Description : 清除session中的实体对象
     *@param : [entity] 实体对象
     *@return: void
     *@Date : 2017/3/25 13:32
    */
    public void evictFromSession(T entity){

        try{
            Session session = sessionFactory.getCurrentSession();
            session.evict(entity);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    /*
     *@Author : wuqingzhou
     *@Description : 根据hql语句进行分页查询
     *@param : [hql, page, pageSize]hql语句  当前页面  每页显示数量
     *@return: java.util.List<T>
     *@Date : 2017/3/25 13:34
     */
    @Override
    public <T> List<T> findForPage(String hql, int page, int pageSize) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql);
        int firstResult = (page - 1) * pageSize;
        query.setFirstResult(firstResult);
        query.setMaxResults(pageSize);
        List<T> list=query.list();
        return list;
    }
    /*
     *@Author : wuqingzhou
     *@Description : 根据hql语句统计实体数量
     *@param : [hql] hql语句
     *@return: int 实体数量
     *@Date : 2017/3/25 13:35
     */
    @Override
    public int getCount(String hql) {
        int total=0;
        List list = getHibernateTemplate().find(hql);
        if(list.size()>0){
            total = list.size();
        }
        return total;

    }
    /*
     *@Author : wuqingzhou
     *@Description : 根据sql统计总条数
     *@param : [sql]sql语句
     *@return: int 返回统计的条数
     *@Date : 2017/3/25 13:45
     */
    @Override
    public int getSQLCount(final String sql) {
        int total=0;
        Session session = this.getSession();
        Query query = session.createSQLQuery(sql);
        List list = query.list();
        if(list.size()>0){
            total = list.size();
        }
        return total;

    }
    /*
     *@Author : wuqingzhou
     *@Description : 根据hql  ,多条件组合语句查询单个对象
     *@param : [hql, params]  hql语句  多个条件
     *@return: T  单个实体对象
     *@Date : 2017/3/25 13:47
     */
    @Override
    public <T> T findUniqueEntity(final String hql, final Object... params) {
        if (StringUtils.isEmpty(hql)){
            throw new IllegalArgumentException("queryString is null");
        }
        if (ObjectUtils.isEmpty(params)){
            return (T)getSession().createQuery(hql).uniqueResult();
        }else{
            Query query = getSession().createQuery(hql).setCacheable(true);
            for (int i = 0; i < params.length; i++) {
                query.setParameter(i, params[i]);
            }
            return (T)query.uniqueResult();
        }
    }
    /*
     *@Author : wuqingzhou
     *@Description : 根据hql  ,多条件组合语句查询任意对象的集合
     *@param : [queryString, values]hql语句  多条件组合
     *@return: java.util.List<T> 任意对象的集合
     *@Date : 2017/3/25 13:43
     */
    @Override
    public List<T> findByList(String hql, Object[] values) {
        if (ObjectUtils.isEmpty(values)){
            return find(hql);
        }else{
            Query query = getSession().createQuery(hql).setCacheable(true);
            for (int i = 0; i < values.length; i++){
                query.setParameter(i, values[i]);
            }
            return query.list();
        }
    }
    /*
     *@Author : wuqingzhou
     *@Description :根据hql语句查询任意对象的集合
     *@param : [hql]  hql语句
     *@return: java.util.List<T> 任意对象的集合
     *@Date : 2017/3/25 13:40
     */
    @Override
    public <T> List<T> findByCurr(String hql) {
        return getSession().createQuery(hql).setCacheable(true).list();
    }

    @Override
    public <T> List<T> findByCurr (String hql,String paramName,Collection list) {
        Query query = getSession().createQuery(hql);
        query.setParameterList(paramName, list);
        return query.list();
    }
    /*
     *@Author : wuqingzhou
     *@Description : 根据hql  ,多条件组合语句批量删除或者修改
     *@param : [hql, values]hql语句  多条件数组
     *@return: int  批量修改或者删除影响的条数
     *@Date : 2017/3/25 13:52
     */
    public int bulkUpdate(String hql, Object[] values){
        Query query = getSession().createQuery(hql);
        for(int i = 0; i < values.length; i++){
            query.setParameter(i, values[i]);
        }
        try{
            return query.executeUpdate();
        }catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }
    /*
     *@Author : wuqingzhou
     *@Description : 根据hql  ,多条件组合语句查询任意对象的集合
     *@param : [queryString, values] hql语句 多条件数组
     *@return: java.util.List<?> 返回任意对象的集合
     *@Date : 2017/3/25 13:56
     */
    @Override
    public List<?> find(String hql, Object[] values) {
        if (ObjectUtils.isEmpty(values)){
            return find(hql);
        }else{
            Query query = getSession().createQuery(hql).setCacheable(true);
            for (int i = 0; i < values.length; i++){
                query.setParameter(i, values[i]);
            }
            return query.list();
        }
    }
    /*
      *@Author : wuyanpeng
      *@Description : 批量执行sql语句
      *@param : sqlList sql语句集合
      *@return: void
      *@Date : 2017/9/26 10:40
      */
    @Override
    public void excuteSQL(final List<String> sqlList){
        if(sqlList==null || sqlList.size()==0){
            return;
        }
        Session session = this.getSession();
        session.doReturningWork(new ReturningWork<ResultSet>() {
            @Override
            public ResultSet execute(java.sql.Connection connection) throws SQLException{
                Statement statement=connection.createStatement();
                String curretnSql = "";

                try{
                    connection.setAutoCommit(false);
                    for (int i=0;i<sqlList.size();i++) {
                        //curretnSql = sql;
                        statement.addBatch(sqlList.get(i));
                        //statement.execute(sql);
                        //System.out.println("执行sql语句：\r\n"+curretnSql+"\r\n时未出现异常");
                        if(i > 0 &&i%1000==0){
                            statement.executeBatch();
                            connection.commit();
                            statement.clearBatch();
                        }
                    }
                    statement.executeBatch();
                    connection.commit();
                }catch (Exception e){
                    //System.out.println("执行sql语句：\r\n"+curretnSql+"\r\n时出现异常");
                    connection.rollback();
                    e.printStackTrace();
                }finally {
                    statement.close();
                    return null;
                }
            }
        });
    }

    @Override
    public void preparedStatementexcuteSQL(final String sql,final List<List<String>> valueList) {
        Session session = this.getSession();
        ResultSet resultSet =session.doReturningWork(new ReturningWork<ResultSet>() {
            @Override
            public ResultSet execute(java.sql.Connection connection) throws SQLException {
                PreparedStatement preparedStatement = null;
                List<ResultSet> resultSets =new ArrayList<ResultSet>();
                try {
                    long start = System.currentTimeMillis();
                    preparedStatement = connection.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
                    ResultSet resultSet=null;
                    for (int i = 0; i < valueList.size(); i++) {
                        for(int j=0;j<valueList.get(i).size();j++){
                            preparedStatement.setString(j+1,valueList.get(i).get(j));
                            System.out.println("第"+i+"行第"+j+"列"+valueList.get(i).get(j));
                        }
                        //批处理
                        preparedStatement.addBatch();
                        //每执行五百条数据操作执行一次批处理，并且将批处理清空
                        if (i!=0&&(i % 1000 == 0)) {
                            preparedStatement.executeBatch();
                            preparedStatement.clearBatch();
                        }
                        //获取自增主键
                        resultSet=preparedStatement.getGeneratedKeys();
                        resultSets.add(resultSet);
                    }
                    //将最后剩余的数据操作进行处理
                    preparedStatement.executeBatch();
                    preparedStatement.clearBatch();
                    resultSet=preparedStatement.getGeneratedKeys();
                    resultSets.add(resultSet);
                    long end = System.currentTimeMillis();
                    connection.commit();
                    System.out.println("批量方法添加十万条数据用时：" + (end - start) + "毫秒");
                } catch (Exception e) {
                    e.printStackTrace();
                    connection.rollback();
                } finally {
                    preparedStatement.close();
                }
                return null;
            }
        });
    }

    @Override
    /**
     * @author lq
     * @date 2018/4/25 9:42
     * @param [sql]
     * @return java.lang.Number
     * @description  分页中获取count方法
     */
    public Number getSQLCountByDateBase(String sql) {
        Session session = this.getSession();
        Query query = session.createSQLQuery("select count(*)"+sql);
        List list = query.list();
        if(CollectionUtils.isEmpty(list)){
            return 0;
        }
        return (Number) list.get(0);
    }

    /*
             *@Author : wuyanpeng
             *@Description : 查询特定字段，结果封装成以map
             *@param : tableName 表名
             *@param : infoItems 要查询的字段名集合
             *@param : condition 查询的条件 key为字段名，value为字段值
             *@return: 返回查询结果的map集合
             *@Date : 2017/9/26 10:45
             */
    public List<Map<String,String>> queryMap(String tableName, List<String> infoItems, Map<String,String> condition){
        String hql = "Select new Map(";
        int size = infoItems.size();
        for (String info:infoItems) {
            size--;
            hql+="b."+info+" as "+info+(size==0?"":",");
        }
        hql+=") From "+tableName+" b where ";
        size = condition.size();
        for (String key:condition.keySet()) {
            size--;
            hql+=" b."+key+"='"+condition.get(key)+"'"+(size==0?"":" and");
        }
        System.out.println("hql is "+hql);
        return find(hql);
    }
    /**
     * @Author : wuyanpeng
     * @Description : 根据对象，获取该对象插入到数据库中的sql语句，
     * @Date_Created : 2017/9/26 10:48
     * @Param excludeFields 忽略的对象属性（有些对象的属性是一个集合或者一个对象，这类属性应该被忽略）
     * @Param SpecialField  为特殊的字段制定值，上一参数中忽略的对象属性在数据库中可能对应一id字段，可在此参数中为该字段指定值，key表示字段名，value表示字段值
     * @Return :返回sql语句
     */
    public String getInsetSqlFromObject(Object o,List<String> excludeFields,Map<String,String> SpecialField) {
        String tableName = o.getClass().getSimpleName();
        String sql = "insert into "+tableName+" (";
        Field[] fields = o.getClass().getDeclaredFields();
        int size = fields.length;
        for(Field f:fields){
            size--;
            f.setAccessible(true);
            try {
                if(excludeFields!=null) {
                    if (f.get(o) != null && !excludeFields.contains(f.getName())) {
                        sql += f.getName() + ",";
                    }
                }else{
                    if (f.get(o) != null) {
                        sql += f.getName() + ",";
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        if(SpecialField!=null) {
            for (String key : SpecialField.keySet()) {
                sql += key + ",";
            }
        }
        sql=sql.substring(0,sql.length()-1)+") ";
        sql+="values(";
        for(Field f:fields){
            f.setAccessible(true);
            try{
                if (f.get(o) != null && !excludeFields.contains(f.getName())) {
                    sql += "'" + f.get(o) + "',";
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        if(SpecialField!=null) {
            for (String key : SpecialField.keySet()) {
                //billHead.getClass().getDeclaredField("id").getGenericType().toString().equals("class java.lang.Long")
                sql += "" + SpecialField.get(key) + ",";
            }
        }
        sql=sql.substring(0,sql.length()-1)+")";
        return sql;
    }
    /**
     * @Author : wuyanpeng
     * @Description : 根据对象，获取该对象更新到数据库中的sql语句，
     * @Date_Created : 2017/9/26 10:48
     * @Param excludeFields 忽略的对象属性（有些对象的属性是一个集合或者一个对象，这类属性应该被忽略）
     * @Param condition  条件，key表示字段名，value表示字段值
     * @Return :
     */
    public String getUpdateSqlFromObject(Object o,List<String> excludeFields,Map<String,String> specialField,Map<String,String> condition){
        String tableName = o.getClass().getSimpleName();
        String sql = "update "+tableName+" set ";
        Field[] fields = o.getClass().getDeclaredFields();
        int size = fields.length;
        for(Field f:fields){
            f.setAccessible(true);
            try {
                if(f.get(o)!=null && !excludeFields.contains(f.getName())){
                    sql += f.getName() + "='" + f.get(o) + "',";
                }else if(f.get(o)==null && !excludeFields.contains(f.getName())){
                    sql += f.getName() + "=" + f.get(o) + ",";
                }
            }catch (Exception e){

            }
        }
        if(specialField!=null){
            for(String key:specialField.keySet()){
                sql+=key+"='"+specialField.get(key)+",'";
            }
        }
        sql = sql.substring(0,sql.length()-1);
        sql+=" where ";
        if(condition!=null){
            for(String key:condition.keySet()){
                sql+=key+"='"+condition.get(key)+"' and ";
            }
        }
        sql = sql.substring(0,sql.length()-4);
        return sql;
    }
}
