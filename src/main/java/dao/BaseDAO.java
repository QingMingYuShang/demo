package dao;


import org.hibernate.Session;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @Author : lq
 * @Description :baseDao接口
 * @Date : Created in 12:57 2017/3/25.
 */
public interface BaseDAO<T, ID extends Serializable>{

    /**
     * 保存单个实体
     * @param entity
     */
    boolean save(T entity);
    /**
     * 修改单个实体
     * @param entity
     */
    void update(T entity);
    /**
     * 保存并更新实体
     * @param entity
     */
    void merge(T entity);

    T mergeAndReturnObject(T entity);
    /**
     * 保存或者修改单个实体
     * @param entity
     */
    void saveOrUpdate(T entity);
    /**
     * 删除实体
     * @param entity
     */
    void delete(T entity);
    /**
     * 依据类的Class删除所有实体数据
     * @param tClass
     */
    void deleteAll(Class<T> tClass);
    /**
     * 批量删除数据
     */
    void deleteAll(Collection<T> entities);
    /**
     * 批量删除数据
     */
    void deleteAllByIds(Class<T> tClass, String conditionName, Collection conditionValues);
    /**
     * 根据类名和字段名条件删除
     */
    void deleteByCondition(Class<T> tClass, String conditionName, String conditionValue);
    /**
     * 批量保存实体对象
     * @param objectList 实体对象的集合
     * */
    //void insertAll(List<T> objectList);
    void mergeAll(List<T> objectList);
    /**
     * 批量保存实体对象
     * @param objectList 实体对象的集合
     * */
    void saveAll(List<T> objectList);
    /**
     * 批量更新实体对象
     * @param objectList 实体对象的集合
     * */
    void updateAll(List<T> objectList);
    /**
     *根据类的Class  和 实体对象的ID 删除单个实体
     * @param tClass 类的Class
     * @param id 实体对象的ID
     */
    void removeByID(Class<T> tClass, ID id);
    /**
     * HQL语句查询单个实体
     * @param hql  hql语句
     * @return
     */
    T get(String hql);
    /**
     * 根据hql语句和对象的实例，获取单个对象
     * @param hql hql语句
     * @param obj 实体对象
     * @return  返回一个对象
     */
    Object get(String hql, Object obj);
    /**
     * 根据hql语句查询一个对象的集合
     * @param hql
     * @return 返回一个对象的集合
     */
    List<T> find(String hql);

    /**
     * 查找符合实体bean数据要求的数据
     * @param bean
     * @return
     */
    List<T> find(T bean);

    /**
     * 根据hql语句查询字段在集合listStr中对象的集合
     * @param hql
     * @param list
     * @return
     */
    List<T> find(String hql, String paramName, Collection list);
    /**
     *根据hql语句和对象的实例 ，获取一个对象的集合
     * @param hql hql语句
     * @param obj  任意对象
     * @return  返回一个对象的集合
     */
    List find(String hql, Object obj);
    /**
     *根据类的Class  和  对象的ID查询单个对象
     * @param entity 类的Class
     * @param id 对象的ID
     * @return 返回单个对象
     */
    Object findById(Class<?> entity, ID id);
    /**
     *根据类的Class 查询所有的实例
     * @param classEntity 类的Class
     * @return 返回所有的实例对象
     */
    List<T> findAll(Class<?> classEntity);
    /**
     * 根据sql语句查询任意对象
     * @param sql  sql语句
     * @return  任意对象的集合
     */
    List<?> findSQL(final String sql);
    /**
     * 根据sql语句进行分页查询
     * @param sql  sql语句
     * @param page  当前页数
     * @param pageSize 每页显示最大数量
     * @return  任意对象的集合
     */
    List<T> findSQLPage(final String sql, int page, int pageSize);
    /**
     * 根据hql语句统计数据的条数
     *@param hql  hql语句
     * */
    int count(String hql);
    /**
     * 清除session中的实体对象
     *@param entity  实体对象
     * */
    void evictFromSession(T entity);

    /**
     * 根据hql语句进行分页查询
     *@param hql  hql语句
     *@param page  当前页面
     *@param pageSize  每页显示数量
     * */
    <T> List<T> findForPage(String hql, int page, int pageSize);
    /**
     * 根据hql语句统计实体数量
     *@param hql  hql语句
     * */
    int getCount(String hql);
    /**
     * 根据hql语句查询任意对象的集合
     *@param hql  hql语句
     * */
    public <T> List<T> findByCurr(String hql);

    /**
     * 查询参数符合条件的集合
     * @param hql
     * @param paramName 参数名
     * @param list 参数集合
     * @param <T>
     * @return
     */
    <T> List<T> findByCurr(String hql, String paramName, Collection list);
    /**
     * 根据hql  ,多条件组合语句查询任意对象的集合
     *@param hql  hql语句
     *@param values 多条件数组
     * */
    public List<T> findByList(String hql, Object[] values);
    /**根据sql统计总条数
     * @param sql sql语句
     * @return  返回统计的条数
     */
    int getSQLCount(String sql);
    /*
     *根据hql  ,多条件组合语句查询单个对象
     *@param  hql 查询语句
     *@param param 查询参数
     @return  单个实例对象
     */
    public <T> T findUniqueEntity(String hql, Object... params);
    /**
     * 根据hql  ,多条件组合语句批量删除或者修改
     * @param hql hql语句
     * @param values  多条件数组
     * @return 返回批量修改或者删除的条数
     */
    public int bulkUpdate(String hql, Object[] values);
    /**
     * 执行sql语句，更新字段
     * @param sql hql语句
     * @return
     */
    public void updateSQL(String sql);
    /**
     * 根据hql  ,多条件组合语句查询任意对象的集合
     * @param hql hql语句
     * @param values 多条件数组
     * @return  返回任意对象的集合
     */
    public List<?> find(String hql, Object[] values);
    void excuteSQL(final List<String> sqlList);

    List<Map<String,String>> queryMap(String tableName, List<String> queryInfos, Map<String, String> conditionInfos);
    /**
     * @Author : wuyanpeng
     * @Description : 根据对象，获取该对象插入到数据库中的sql语句，
     * @Date_Created : 2017/9/26 10:48
     * @Param excludeFields 忽略的对象属性（有些对象的属性是一个集合或者一个对象，这类属性应该被忽略）
     * @Param SpecialField  为特殊的字段制定值，上一参数中忽略的对象属性在数据库中可能对应一id字段，可在此参数中为该字段指定值，key表示字段名，value表示字段值
     * @Return :返回sql语句
     */
    String getInsetSqlFromObject(Object o, List<String> excludeFields, Map<String, String> SpecialField);
    String getUpdateSqlFromObject(Object o, List<String> excludeFields, Map<String, String> specialField, Map<String, String> condition);


    Session getSession();
    void preparedStatementexcuteSQL(final String sql, final List<List<String>> sqlList);

    Number getSQLCountByDateBase(String sql);
}
