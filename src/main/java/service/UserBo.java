package service;

import model.User;

import java.util.List;
import java.util.Map;


/**
 * Created by lq on 2018/8/20.
 */
public interface UserBo {
    public List<Map> queryUser(String userName);
    List<User> findUser();
}
