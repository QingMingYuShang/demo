package service.impl;

import dao.BaseDAO;
import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import service.UserBo;

import java.util.List;
import java.util.Map;
/**
 * Created by lq on 2018/8/20.
 */
@Service("userBo")
public class UserBoImpl implements UserBo{
    @Autowired
    private BaseDAO<User,Long> userDao;

    @Override
    /**
     * @author lq
     * @date 2018/8/20 10:42
     * @param [userName]
     * @return model.User
     * @description 通过用户名查询
     */
    public List<Map> queryUser(String userName) {
        System.out.println(userName);
        StringBuffer sb = new StringBuffer("select new Map (s.id as id,s.userName as userName,s.passWord as passWord) From User s where userName= '"+userName+"'");
        System.out.println();
        List<Map> user = userDao.findByCurr(sb.toString());
        return user;
    }

    @Override
    /**
     * @author lq
     * @date 2018/8/20 18:20
     * @param []
     * @return java.util.List<model.User>
     * @description 通过查询用户
     */
    public List<User> findUser() {
        return  userDao.find("From User where userName ");
    }


}
