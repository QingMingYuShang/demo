package controller;

import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import service.UserBo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author lq
 * @date 2018/8/13 14:40
 * @param
 * @return
 * @description 测试
 */
@Controller
@RequestMapping("/home")
public class IndexController {
    @Autowired
    private UserBo userBo;

    @RequestMapping("/index")
    public String index(HttpServletRequest request, HttpServletResponse response)throws Exception {
        return "hello";
    }

    @RequestMapping(value = "/getJson",method = RequestMethod.GET)
    public @ResponseBody User getJson(){
        User user = new User();
        user.setUserName("小李");
        user.setPassWord("123456");
        user.setCreateDate(new Date());
        return user;
    }
    @RequestMapping(value = "/getUserName",method = RequestMethod.POST,produces="application/json; charset=UTF-8")
    public @ResponseBody User[] getUserName(@RequestBody User[] users){
        for (User user1 : users) {
            System.out.println(user1.getPassWord());
            System.out.println(user1.getUserName());
            System.out.println(user1.getCreateDate());
        }
        return users;
    }
    @RequestMapping(value = "/getUser")
    public @ResponseBody List<Map> getUser( String userName, String passWord){
        List<Map> user= null;
        try {
            System.out.println(userName);
            System.out.println("ceshi");
            System.out.println(passWord);
            user = userBo.queryUser(userName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return user;
    }


    /**
     * @author lq
     * @date 2018/8/20 18:21
     * @param
     * @return
     * @description
     */
    @RequestMapping(value = "/findUser")
    public @ResponseBody List<User> findUser(){
        List<User> user =  userBo.findUser();
        return user;
    }
}
