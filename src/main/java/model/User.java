package model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * @author lq
 * @date 2018/8/15 9:34
 * @param
 * @return
 * @description 用户信息
 */
@Component
@Entity
@Table
public class User  implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false,columnDefinition = " bigint(20) comment '用户id'")
    private Long id;
    @Column()
    private String userName;
    @Column()
    private String passWord;
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @Column()
    private Date createDate;
    private Integer sb;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Integer getSb() {
        return sb;
    }

    public void setSb(Integer sb) {
        this.sb = sb;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
