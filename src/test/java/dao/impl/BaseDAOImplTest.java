package dao.impl;

import org.junit.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import static org.junit.Assert.*;

/**
 * Created by lq on 2018/8/23.
 */
public class BaseDAOImplTest {
    @Test
    public void getSession() throws Exception {
        Class.forName("com.mysql.jdbc.Driver");
        Connection connection = DriverManager.getConnection("jdbc:mysql://47.107.44.159:3306/test?useSSL=false&characterEncoding=UTF-8", "test", "123456");
        Statement statement=connection.createStatement();
        ResultSet set=statement.executeQuery("select * from User ");
        while (set.next())
        {
            System.out.println(set.getString(4));
            System.out.println(set.getString(5));
        }
    }

}